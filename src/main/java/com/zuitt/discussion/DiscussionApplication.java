package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@SpringBootApplication
@RestController
public class DiscussionApplication {
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}


	// get all users
	@GetMapping("/users")
	public String getAllUser(){
		return "All user retrieved";
	}
	// create all user
	@PostMapping("/users")
	public String createUser(){
		return "New user created.";
	}
	// get specific user
	@GetMapping("/users/{id}")
	public String getUser(@PathVariable Long id){
		return "Hello "+id;
	}

	// delete specific user
	@DeleteMapping("/users/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable Long id, @RequestHeader("Authorization") String user) {
		if (user != null && !user.isEmpty()) {
			// perform delete operation through name
			return ResponseEntity.ok().body("The User " + id + " has been deleted.");
		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized access.");
		}
	}
	@PutMapping("/users/{id}")
	@ResponseBody
	public Users updateName(@PathVariable Long id, @RequestBody Users user) {
		// update the name of the user
		return user;
	}

}

/*	FROM DISCUSSION

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	//Retrieve all posts
	//localhost:8080/posts
	@RequestMapping(value="/posts",method = RequestMethod.GET)
	public String getPosts(){
		return "All posts retrieved.";
	}

	//Creating a new post
	//localhost:8080/posts
	@RequestMapping(value = "/posts", method = RequestMethod.POST)
	public String createPost(){
		return "New post created.";
	}

	//Retrieving a single post
	//localhost:8080/posts/
	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.GET)
	public String getPost(@PathVariable Long postid){
		return "Viewing details of post " + postid;
	}

	//Delete a post
	//localhost:8080/posts/1234
	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
	public String deletePost(@PathVariable Long postid){
		return "The post " + postid + " has been deleted";
	}

	//Updating a post
	//localhost:8080/posts/1234
	@RequestMapping(value = "/posts/{postid}", method=RequestMethod.PUT)
	@ResponseBody
	public Post updatePost(@PathVariable Long postid, @RequestBody Post post){
		return post;
	}

	//	Retrieving a posts for a particular user.
//	localhost:8080/myPosts
//	@RequestMapping(value = "/myPosts", method = RequestMethod.GET)
	@GetMapping("/myPosts")
	public String getMyPosts(@RequestHeader(value = "Authorization") String user ){
		return "Posts for " + user + " have been retrieved";
	}


 */